import { defineStore, storeToRefs } from "pinia"
import { ref, computed, onMounted, onBeforeMount, onUpdated, watch } from "vue"
import { getData, updateData } from "../assets/js/api"
import { useAutoRefreshStore } from "./autoRefreshStore.js"

export const useTopicStore = defineStore("TopicStore", () => {
  // =========================================== DATA
  const debug = true

  const topics = ref([
    {
      name: "reflection",
      isVisible: true,
      opacity: 1,
      items: [{ isVisible: true }, { isVisible: false }],
    },
  ])

  function toggleLockAutoRefresh() {
    const { isLocked } = storeToRefs(useAutoRefreshStore())
    isLocked.value = !isLocked.value
  }

  watch(
    topics,
    (newTopics) => {
      toggleLockAutoRefresh()
      updateData("topics", topics.value).then((data) => {
        console.log('updateData from topics watch : ', data)
        toggleLockAutoRefresh()
      })
    },
    { deep: true }
  )

  // =========================================== INIT FUNCTIONS
  // getData("topics").then((data) => {
  //   topics.value = data
  // })

  // =========================================== COMPUTED
  const visibleTopic = computed(() => {
    return topics.value.find((topic) => topic.isVisible)
  })
  const visibleItem = computed(() => {
    return visibleTopic.value.items.find((item) => item.isVisible)
  })

  // =========================================== FUNCTIONS
  function nextItem() {
    let nextVisibleIndex
    visibleTopic.value.items.forEach((item, index) => {
      if (item.isVisible) {
        item.isVisible = false
        nextVisibleIndex =
          visibleTopic.value.items.indexOf(item) + 1 >
          visibleTopic.value.items.length - 1
            ? 0
            : visibleTopic.value.items.indexOf(item) + 1
      }
    })
    visibleTopic.value.items[nextVisibleIndex].isVisible = true
  }

  function toggleTopic(topic) {
    const isVisible = topic.isVisible
    hideAllTopics()
    if (!isVisible) topic.isVisible = true
  }

  function hideAllTopics() {
    topics.value = topics.value.map((topic) => {
      topic.isVisible = false
      return topic
    })
  }

  return {
    topics,
    visibleTopic,
    visibleItem,
    nextItem,
    toggleTopic,
  }
})

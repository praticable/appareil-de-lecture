import { defineStore, storeToRefs } from "pinia"
import { ref, watch, computed } from "vue"
import { updateData } from "../assets/js/api"
import { useAutoRefreshStore } from "../stores/autoRefreshStore.js"

export const useNavStore = defineStore("NavStore", () => {
  const nav = ref("Nav")

  function toggleLockAutoRefresh() {
    const { isLocked } = storeToRefs(useAutoRefreshStore())
    isLocked.value = !isLocked.value
  }

  watch(
    nav,
    (newNav) => {
      if (nav.value && nav.value !== null && nav.value.length > 0) {
        toggleLockAutoRefresh()
        updateData("nav", nav.value).then((data) => {
          toggleLockAutoRefresh()
        })
      }
    },
    { deep: true }
  )

  return { nav }
})

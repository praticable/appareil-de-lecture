import { defineStore, storeToRefs } from "pinia"
import { ref, computed, onMounted, onBeforeMount, onUpdated, watch } from "vue"
import { getData, updateData } from "../assets/js/api"
import { useAutoRefreshStore } from "../stores/autoRefreshStore.js"

export const useAnnotationStore = defineStore("AnnotationStore", () => {
  // =========================================== DATA
  const debug = true

  const annotations = ref([
    {
      name: "espace",
      isVisible: true,
      opacity: 1,
      images: [
        { fileName: "0.jpg", isVisible: true, audioFileName: false },
        { fileName: "1.png", isVisible: false, audioFileName: "1-1.mp3" },
        { fileName: "2.png", isVisible: false, audioFileName: false },
        { fileName: "3.png", isVisible: false, audioFileName: "1-3.mp3" },
      ],
    },
    {
      name: "profanation",
      isVisible: false,
      opacity: 1,
      images: [
        { fileName: "0.jpg", isVisible: true, audioFileName: false },
        { fileName: "1.png", isVisible: false, audioFileName: false },
        { fileName: "2.png", isVisible: false, audioFileName: false },
        { fileName: "3.png", isVisible: false, audioFileName: false },
        { fileName: "4.png", isVisible: false, audioFileName: false },
      ],
    },
  ])

  function toggleLockAutoRefresh() {
    const { isLocked } = storeToRefs(useAutoRefreshStore())
    isLocked.value = !isLocked.value
  }

  // =========================================== INIT FUNCTIONS
  // getData("annotations").then((data) => {
  //   annotations.value = data
  // })

  // =========================================== COMPUTED
  const visibleGroup = computed(() => {
    return annotations.value.find((group) => group.isVisible)
  })

  // =========================================== FUNCTIONS
  function toggleLayer(groupName, imageIndex) {
    toggleLockAutoRefresh()
    setTimeout(() => {
      const targetGroup = annotations.value.find(
        (group) => group.name === groupName
      )
      targetGroup.images[imageIndex].isVisible =
        !targetGroup.images[imageIndex].isVisible
      
        updateData("annotations", annotations.value).then((data) => {
          toggleLockAutoRefresh()
        })
    }, 100)
  }

  function prevGroup() {
    const visibleGroupIndex = annotations.value.indexOf(visibleGroup.value)
    visibleGroup.value.isVisible = false
    if (visibleGroupIndex > 0) {
      annotations.value[visibleGroupIndex - 1].isVisible = true
    } else {
      annotations.value[annotations.value.length - 1].isVisible = true
    }
    toggleLockAutoRefresh()
      updateData("annotations", annotations.value).then((data) => {
        toggleLockAutoRefresh()
      })
  }
  function nextGroup() {
    const visibleGroupIndex = annotations.value.indexOf(visibleGroup.value)
    visibleGroup.value.isVisible = false
    if (visibleGroupIndex < annotations.value.length - 1) {
      annotations.value[visibleGroupIndex + 1].isVisible = true
    } else {
      annotations.value[0].isVisible = true
    }
    toggleLockAutoRefresh()
      updateData("annotations", annotations.value).then((data) => {
        toggleLockAutoRefresh()
      })
  }

  return { annotations, visibleGroup, toggleLayer, prevGroup, nextGroup }
})

import { defineStore, storeToRefs } from "pinia"
import { ref } from "vue"
import { getData, updateData } from "../assets/js/api"
import { useAnnotationStore } from "./annotationStore.js"
import { useNavStore } from "./navStore.js"
import { useTopicStore } from "./topicStore"

export const useAutoRefreshStore = defineStore("AutoRefreshStore", () => {
  const { nav } = storeToRefs(useNavStore())
  const { annotations } = storeToRefs(useAnnotationStore())
  const { topics } = storeToRefs(useTopicStore())

  const isLocked = ref(false)

  function autoRefresh(isAbleToUpdateData = false) {
    setInterval(() => {
      getData().then((data) => {
        if (!data.nav && isAbleToUpdateData) {
          const currentApp = eval(nav.value.toLowerCase())
          console.log('autoRefresh then getData, no data.nav, update to : ', nav.value )
          updateData('nav', currentApp.value)
            .then(data => {
              console.log('update data from autorefresh.getData : ', data)
            })
        } else {
          nav.value = data.nav
          if (!isLocked.value) {
            console.log("refresh")
            updateStore(nav.value, data)
          } else {
            console.log("locked")
          }
        }
      })
    }, 250)
  }

  function updateStore(appName, data) {
    if (appName && typeof appName === "string") {
      console.log('appName', appName)
      appName = appName.toLowerCase()
      const storeName = appName
      const store = eval(storeName)
      if (JSON.stringify(store) !== JSON.stringify(data[appName])) {
        store.value = data[appName]
      }
    }
  }

  return { autoRefresh, isLocked }
})

import { updateData } from "./updateData.js"
import { getData } from "./getData.js"
import { restoreData } from "./restoreData.js"

export { getData, updateData, restoreData }

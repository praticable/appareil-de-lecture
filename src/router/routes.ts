import Control from "../views/Control.vue"
import Display from "../views/Display.vue"

const routes = [
  {
    path: "/display",
    name: "Display",
    component: Display,
  },
  {
    path: "/control",
    name: "Control",
    component: Control,
  },
]

export default routes
